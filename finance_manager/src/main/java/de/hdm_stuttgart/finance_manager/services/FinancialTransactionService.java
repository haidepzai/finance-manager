package de.hdm_stuttgart.finance_manager.services;

import de.hdm_stuttgart.finance_manager.model.Expense;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class FinancialTransactionService {

    @PersistenceContext
    private EntityManager em;

    public void addExpense(Expense e) {
        em.persist(e);
    }

    public void updateExpense(Expense e){

    }

    public void removeRecord(Expense e){

    }

    public Expense findById(Long id){
        return null;
    }

    public List<Expense> getAllExpenses() {
        return null;
    }
}