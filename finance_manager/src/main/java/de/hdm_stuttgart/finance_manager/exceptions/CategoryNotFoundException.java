package de.hdm_stuttgart.finance_manager.exceptions;

public class CategoryNotFoundException extends RuntimeException {
    public CategoryNotFoundException(Long id) {
        super("Could not find Category with id " + id);
    }
}
