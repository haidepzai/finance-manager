package de.hdm_stuttgart.finance_manager.validator;

import de.hdm_stuttgart.finance_manager.model.Expense;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.math.BigDecimal;

@Component("beforeCreateExpenseValidator")
public class ExpenseValidator extends FinancialRecordValidator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Expense.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        if (o instanceof Expense e) {
            if(checkAmount(e.getAmount())) {
                errors.rejectValue("amount", "Negative Amount");
            }
        }

    }

    private boolean checkAmount(BigDecimal a) {
        return (a == null || a.doubleValue() < 0.00);
    }
}
