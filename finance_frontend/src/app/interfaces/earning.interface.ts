import { PaymentType } from '../enums/payment-type.enum';
import { Category } from './category.interface';

export interface Earning {
  id: number;
  label: string;
  date: Date;
  amount: number;
  category: Category;
  paymentType: PaymentType;
  source: string;
}

export type CreateEarningDto = Omit<Earning, 'id'>;

export type UpdateEarningDto = Omit<Partial<Earning>, 'id'>;
