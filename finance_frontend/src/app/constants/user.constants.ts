import { User } from '../interfaces/user.interface';

export const USER_MOCK: User = {
  id: 1,
  name: 'Jane Doe',
  isAdmin: true,
};
