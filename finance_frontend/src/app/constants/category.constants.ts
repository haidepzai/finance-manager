import { Category } from '../interfaces/category.interface';

export const CATEGORIES_MOCK: Category[] = [
  {
    id: 1,
    label: 'Lebensmittel',
  },
  {
    id: 2,
    label: 'Kleidung',
  },
  {
    id: 3,
    label: 'Versicherungen',
  },
  {
    id: 4,
    label: 'Freizeit',
  },
  {
    id: 5,
    label: 'Arbeit',
  },
];
