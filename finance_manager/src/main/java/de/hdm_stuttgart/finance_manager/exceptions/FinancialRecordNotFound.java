package de.hdm_stuttgart.finance_manager.exceptions;

public class FinancialRecordNotFound extends RuntimeException{
    public FinancialRecordNotFound(Long id){
        super("Keinen Entrag gefunden mit der ID: " + id);
    }
}
