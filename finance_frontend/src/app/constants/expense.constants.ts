import { PaymentType } from '../enums/payment-type.enum';
import { Expense } from '../interfaces/expense.interface';
import { CATEGORIES_MOCK } from './category.constants';

export const EXPENSES_MOCK: Expense[] = [
  {
    id: 1,
    label: 'Zutaten',
    date: new Date('2021-06-07'),
    amount: 18.85,
    recipient: 'ALDI',
    category: CATEGORIES_MOCK[0],
    paymentType: PaymentType.DEBIT,
  },
  {
    id: 2,
    label: 'PS5',
    date: new Date('2021-06-01'),
    amount: 599.99,
    recipient: 'Saturn',
    category: CATEGORIES_MOCK[3],
    paymentType: PaymentType.CARD,
  },
  {
    id: 3,
    label: 'Krankenkasse',
    date: new Date('2021-06-15'),
    amount: 110.0,
    recipient: 'TKK',
    category: CATEGORIES_MOCK[2],
    paymentType: PaymentType.CASH_TRANSFER,
  },
  {
    id: 4,
    label: 'Jacke',
    date: new Date('2021-06-10'),
    amount: 79.99,
    recipient: 'H&M',
    category: CATEGORIES_MOCK[1],
    paymentType: PaymentType.CREDIT_CARD,
  },

  {
    id: 5,
    label: 'Drogerie',
    date: new Date('2021-05-05'),
    amount: 17.63,
    recipient: 'DM',
    category: CATEGORIES_MOCK[0],
    paymentType: PaymentType.CASH,
  },
];
