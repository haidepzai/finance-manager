package de.hdm_stuttgart.finance_manager.repository;

import de.hdm_stuttgart.finance_manager.model.Expense;
import org.springframework.data.repository.CrudRepository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface ExpenseRepository extends CrudRepository<Expense, Long> {
    List<Expense> findByItem(String item);
    Iterable<Expense> findAll(Pageable page);
}
