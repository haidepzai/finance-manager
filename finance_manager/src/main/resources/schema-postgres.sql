DROP TABLE IF EXISTS financial_record;
DROP TABLE IF EXISTS category;
CREATE TABLE category
(
    id BIGSERIAL PRIMARY KEY,
    created_at TIMESTAMP,
    changed_at TIMESTAMP,
    name varchar(255) NOT NULL,
    UNIQUE(name)
);
CREATE TABLE financial_record
(
    dtype varchar(31) NOT NULL,
    id BIGSERIAL PRIMARY KEY,
    created_at TIMESTAMP,
    changed_at TIMESTAMP,
    version INTEGER,
    amount numeric(19,2),
    date date,
    item varchar(255),
    payment_type varchar(255),
    source varchar(255),
    recipient varchar(255),
    category_id bigint references category(id)
);