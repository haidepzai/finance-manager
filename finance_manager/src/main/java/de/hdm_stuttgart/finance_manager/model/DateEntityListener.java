package de.hdm_stuttgart.finance_manager.model;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

public class DateEntityListener{
    @PrePersist
    public void onPrePersist(Object entity) {
        if (entity instanceof DateEntity dateEntity) {
            dateEntity.createdAt = LocalDateTime.now();
        }
    }
    @PreUpdate
    public void onPreUpdate(Object entity) {
        if (entity instanceof DateEntity dateEntity) {
            dateEntity.changedAt = LocalDateTime.now();
        }
    }
}