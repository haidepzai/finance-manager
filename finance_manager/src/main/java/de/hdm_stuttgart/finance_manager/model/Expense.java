package de.hdm_stuttgart.finance_manager.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@DiscriminatorValue("Expenses")
public class Expense extends FinancialRecord {

    @NotNull
    @Size(min = 1, message="Empfänger muss mindestens 1 Zeichen lang sein.")
    private String recipient;

    public Expense() {
    }

    public Expense(
            Date date,
            BigDecimal amount,
            String recipient,
            String item,
            Category category,
            PaymentMethod paymentMethod
    ) {
        super(date, amount, item, category, paymentMethod);
        this.recipient = recipient;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String receiver) {
        this.recipient = receiver;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Expenses{" +
                "recipient='" + recipient + '\'' + "}";
    }
}
