package de.hdm_stuttgart.finance_manager;

import de.hdm_stuttgart.finance_manager.model.Category;
import de.hdm_stuttgart.finance_manager.model.Earning;
import de.hdm_stuttgart.finance_manager.model.Expense;
import de.hdm_stuttgart.finance_manager.model.PaymentMethod;
import de.hdm_stuttgart.finance_manager.repository.CategoryRepository;
import de.hdm_stuttgart.finance_manager.repository.EarningRepository;
import de.hdm_stuttgart.finance_manager.repository.ExpenseRepository;
import de.hdm_stuttgart.finance_manager.repository.FinancialRecordRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

@SpringBootApplication
@EnableJpaAuditing
public class FinanceManagerApplication {

    private static final Logger log = LoggerFactory.getLogger((FinanceManagerApplication.class));

    public static void main(String[] args) {
        SpringApplication.run(FinanceManagerApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(
            ExpenseRepository exRepo,
            CategoryRepository catRepo,
            EarningRepository earnRepo,
            FinancialRecordRepository financeRepo
    ){
        return (args) -> {
            log.info("Creating expense entities...");
            exRepo.save(new Expense(new Date(), new BigDecimal("1000"), "Hai Vu", "Essen", new Category("Leisure"), PaymentMethod.CASH));

            earnRepo.save(new Earning("Hai Vu", new Date(), new BigDecimal("1000"), "Salary", new Category("Leisure"), PaymentMethod.CREDIT));

            Pageable page = PageRequest.of(0,2);

            exRepo.findAll(page).forEach(i -> {
                log.info(i.toString());
            });

        };
    }
}
