import { Expense } from './../../../interfaces/expense.interface';
import { EXPENSES_MOCK } from './../../../constants/expense.constants';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.scss']
})
export class ExpenseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  displayedColumns: string[] = ['id', 'label', 'recipient', 'category', 'paymentType', 'amount', 'actions'];
  expenses = EXPENSES_MOCK;

  onAdd(): void {

  }

  onEdit(expense: Expense) {

  }
}
