package de.hdm_stuttgart.finance_manager.dto;

import de.hdm_stuttgart.finance_manager.model.PaymentMethod;

import java.math.BigDecimal;
import java.util.Date;

public class ExpenseDTO extends FinancialRecordDTO {
    public ExpenseDTO(
            Date date,
            BigDecimal amount,
            String recipient,
            String item,
            Long categoryId,
            PaymentMethod paymentMethod
    ) {
        super(date, amount, item, categoryId, paymentMethod);
        this.recipient = recipient;
    }

    private String recipient;

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }
}
