import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { USER_MOCK } from '../constants/user.constants';
import { StorageKey } from '../enums/storage-key.enum';
import { User } from '../interfaces/user.interface';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private user!: User | null;

  constructor(private router: Router, private storageService: StorageService) {
    this.user = this.storageService.get(StorageKey.USER);
  }

  isLoggedIn(): boolean {
    return !!this.user; //returns either true or false
  }

  isAdmin(): boolean {
    return this.user?.isAdmin || false;
  }

  getUser(): User | null {
    return this.user;
  }

  async login() {
    this.user = USER_MOCK;
    this.storageService.set(StorageKey.USER, USER_MOCK);
    this.router.navigate(['/home']);
  }

  async logout() {
    this.user = null;
    this.storageService.remove(StorageKey.USER);
    this.router.navigate(['/']);
  }
}
