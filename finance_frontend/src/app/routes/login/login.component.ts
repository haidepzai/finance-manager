import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  });

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  onLogin() {
    let username = this.loginForm.get('username')?.value;
    let password = this.loginForm.get('password')?.value;

    console.log(username, password);

    if(username === 'admin' && password === "admin") {
      this.authService.login();
    } else {
      this.loginForm.reset();
    }
  }

}
