package de.hdm_stuttgart.finance_manager.model;

public enum PaymentMethod {
    CASH,
    BANK,
    EC,
    CREDIT
}
