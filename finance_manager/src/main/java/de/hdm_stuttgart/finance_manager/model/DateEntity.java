package de.hdm_stuttgart.finance_manager.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class DateEntity {

    public DateEntity() {

    }

    @CreatedDate
    protected LocalDateTime createdAt;

    @LastModifiedDate
    protected LocalDateTime changedAt;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    @Column(name="OPTLOCK")
    private Integer version;

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public LocalDateTime getModifiedAt() {
        return changedAt;
    }

    public Long getId() {
        return id;
    }

    public Integer getVersion() {
        return version;
    }

    @PreUpdate
    void onPreUpdate() {
        changedAt = LocalDateTime.now();
    }

    @PrePersist
    void onPrePersist() {
        createdAt = LocalDateTime.now();
    }
}
