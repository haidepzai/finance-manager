package de.hdm_stuttgart.finance_manager.repository;

import de.hdm_stuttgart.finance_manager.model.Category;
import de.hdm_stuttgart.finance_manager.model.Expense;
import de.hdm_stuttgart.finance_manager.model.FinancialRecord;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource
public interface FinancialRecordRepository extends CrudRepository<FinancialRecord, Long> {
    List<FinancialRecord> findByCategory(Category category);
    List<FinancialRecord> findByItem(String item);
    List<FinancialRecord> findAllByOrderByDateDesc();

    Optional<FinancialRecord> deleteFinancialRecordById(@NonNull @Param("id") Long id);

    @Query("from FinancialRecord fr order by type(fr)")
    Iterable<FinancialRecord> findByOrderByType();

    @NonNull
    Optional<FinancialRecord> findById(@NonNull @Param("id") Long id);
}
