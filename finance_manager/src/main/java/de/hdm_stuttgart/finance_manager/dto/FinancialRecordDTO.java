package de.hdm_stuttgart.finance_manager.dto;

import de.hdm_stuttgart.finance_manager.model.DateEntity;
import de.hdm_stuttgart.finance_manager.model.PaymentMethod;

import java.math.BigDecimal;
import java.util.Date;

public abstract class FinancialRecordDTO extends DateEntity {
    public FinancialRecordDTO() {}

    public FinancialRecordDTO(
            Date date,
            BigDecimal amount,
            String item,
            Long categoryId,
            PaymentMethod paymentMethod
    ) {
        this.date = date;
        this.amount = amount;
        this.item = item;
        this.categoryId = categoryId;
        this.paymentMethod = paymentMethod;
    }

    protected Long id;
    protected int version;
    protected Date date;
    protected BigDecimal amount;
    protected String item;
    protected Long categoryId;
    protected PaymentMethod paymentMethod;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
