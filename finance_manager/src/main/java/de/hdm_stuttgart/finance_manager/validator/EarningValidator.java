package de.hdm_stuttgart.finance_manager.validator;

import de.hdm_stuttgart.finance_manager.model.Earning;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component("beforeCreateEarningValidator")
public class EarningValidator extends FinancialRecordValidator{
    @Override
    public boolean supports(Class<?> aClass) {
        return Earning.class.equals(aClass);
    }
    @Override
    public void validate(Object o, Errors errors) {
        super.validate(o, errors);
        if(o instanceof Earning e) {
            if(isEmptyString(e.getSource())) {
                errors.rejectValue("source", "empty");
            }
        }
    }
}
