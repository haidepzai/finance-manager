package de.hdm_stuttgart.finance_manager.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

//Wird allgemein aufgerufen, wenn ein financial Record nicht gefunden wurde

@ControllerAdvice
public class FinancialRecordNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(FinancialRecordNotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String financialRecordNotFoundHandler(FinancialRecordNotFound e) {
        return e.getMessage();
    }
}
