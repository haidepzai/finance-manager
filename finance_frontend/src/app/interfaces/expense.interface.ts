import { PaymentType } from '../enums/payment-type.enum';
import { Category } from './category.interface';

export interface Expense {
  id: number;
  label: string;
  date: Date;
  amount: number;
  category: Category;
  paymentType: PaymentType;
  recipient: string;
}

export type CreateExpenseDto = Omit<Expense, 'id'>;

export type UpdateExpenseDto = Omit<Partial<Expense>, 'id'>;
