INSERT INTO category(created_at, changed_at, name) VALUES ('2021-04-01
08:42:10', '2021-04-01 08:42:10', 'Lebensmittel');
INSERT INTO category(created_at, changed_at, name) VALUES ('2021-04-01
08:42:10', '2021-04-01 08:42:10', 'Kleidung');
INSERT INTO category(created_at, changed_at, name) VALUES ('2021-04-01
08:42:10', '2021-04-01 08:42:10', 'Freizeit');
INSERT INTO category(created_at, changed_at, name) VALUES ('2021-04-01
08:42:10', '2021-04-01 08:42:10', 'Wohnen');
INSERT INTO category(created_at, changed_at, name) VALUES ('2021-04-01
08:42:10', '2021-04-01 08:42:10', 'Versicherungen');
INSERT INTO category(created_at, changed_at, name) VALUES ('2021-04-01
08:42:10', '2021-04-01 08:42:10', 'Arbeit');
INSERT INTO category(created_at, changed_at, name) VALUES ('2021-04-01
08:42:10', '2021-04-01 08:42:10', 'Kapital');
-- Earnings:
INSERT INTO financial_record(dtype, created_at, changed_at, version,
                             amount, date, item, payment_type, source, category_id) VALUES
('Earning', '2021-04-30 08:42:10', '2021-04-30 08:42:10', 0, 3500, '2021-
04-30', 'Gehalt', 'TRANSFER', 'Arbeitgeber', (SELECT id FROM category
                                              where name='Arbeit'));
-- Expenses:
INSERT INTO financial_record(dtype, created_at, changed_at, version,
                             amount, date, item, payment_type, recipient, category_id) VALUES
('Expense', '2021-04-01 08:42:10', '2021-04-01 08:42:10', 0, 900, '2021-
04-01', 'Miete', 'TRANSFER', 'Vermieter', (SELECT id FROM category where
        name='Wohnen'));