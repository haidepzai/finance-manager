package de.hdm_stuttgart.finance_manager.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@DiscriminatorValue("Earnings")
public class Earning extends FinancialRecord {

    @NotNull
    @Size(min = 1, message="Quelle muss mindestens 1 Zeichen lang sein.")
    private String source;

    public Earning() {
    }

    public Earning(
            String source,
            Date date,
            BigDecimal amount,
            String item,
            Category category,
            PaymentMethod paymentMethod
    ) {
        super(date, amount, item, category, paymentMethod);
        this.source = source;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String sender) {
        this.source = sender;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Earnings{" +
                "source='" + source + '\'' + "}";
    }
}
