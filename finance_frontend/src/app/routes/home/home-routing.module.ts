import { AdminGuard } from './../../guards/admin.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: 'expenses', loadChildren: () => import('./expense/expense.module').then((m) => m.ExpenseModule) },
      { path: 'earnings', loadChildren: () => import('./earnings/earnings.module').then((m) => m.EarningsModule) },
      {
        path: 'admin',
        loadChildren: () => import('./admin/admin.module').then((m) => m.AdminModule),
        canActivate: [AdminGuard],
      },
      { path: '**', redirectTo: 'expenses' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
