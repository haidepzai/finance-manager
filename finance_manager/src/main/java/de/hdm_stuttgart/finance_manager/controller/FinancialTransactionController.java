package de.hdm_stuttgart.finance_manager.controller;

import de.hdm_stuttgart.finance_manager.dto.EarningDTO;
import de.hdm_stuttgart.finance_manager.dto.ExpenseDTO;
import de.hdm_stuttgart.finance_manager.dto.FinancialRecordDTO;
import de.hdm_stuttgart.finance_manager.exceptions.CategoryNotFoundException;
import de.hdm_stuttgart.finance_manager.exceptions.FinancialRecordNotFound;
import de.hdm_stuttgart.finance_manager.model.Category;
import de.hdm_stuttgart.finance_manager.model.Earning;
import de.hdm_stuttgart.finance_manager.model.Expense;
import de.hdm_stuttgart.finance_manager.model.FinancialRecord;
import de.hdm_stuttgart.finance_manager.repository.CategoryRepository;
import de.hdm_stuttgart.finance_manager.repository.EarningRepository;
import de.hdm_stuttgart.finance_manager.repository.ExpenseRepository;
import de.hdm_stuttgart.finance_manager.repository.FinancialRecordRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FinancialTransactionController {

    private final FinancialRecordRepository financialRecordRepository;
    private ExpenseRepository expenseRepository;
    private EarningRepository earningRepository;
    private CategoryRepository categoryRepository;

    //Dependency Injection
    private FinancialTransactionController(
            FinancialRecordRepository financialRecordRepository,
            ExpenseRepository expenseRepository,
            EarningRepository earningRepository,
            CategoryRepository categoryRepository
    ) {
        this.financialRecordRepository = financialRecordRepository;
        this.expenseRepository= expenseRepository;
        this.earningRepository= earningRepository;
        this.categoryRepository= categoryRepository;
    }

    //Helper Methods

    //getCategoryById: Helper method to retrieve a Category entity given its ID.
    private Category getCategoryById(Long categoryId) {
        return categoryRepository.findById(categoryId).orElseThrow(() -> new CategoryNotFoundException(categoryId));
    }

    private void updateFinancialRecordFromDTO(FinancialRecord fr, FinancialRecordDTO newFinancialRecordDTO) {
        fr.setAmount(newFinancialRecordDTO.getAmount());
        fr.setCategory(getCategoryById(newFinancialRecordDTO.getCategoryId()));
        fr.setDate(newFinancialRecordDTO.getDate());
        fr.setItem(newFinancialRecordDTO.getItem());
        fr.setPaymentMethod(newFinancialRecordDTO.getPaymentMethod());
    }

    private void updateExpenseFromDTO(Expense expense, ExpenseDTO newExpenseDTO) {
        updateFinancialRecordFromDTO(expense, newExpenseDTO);
        expense.setRecipient(newExpenseDTO.getRecipient());
    }

    private void updateEarningFromDTO(Earning earning, EarningDTO newEarningDTO) {
        updateFinancialRecordFromDTO(earning, newEarningDTO);
        earning.setSource(newEarningDTO.getSource());
    }

    //Create-Services
    @PostMapping("/expenses")
    public Expense createExpense(@RequestBody ExpenseDTO newExpenseDTO) { //Im Body = neue Expense Daten
        Expense newExpense = new Expense();
        updateExpenseFromDTO(newExpense, newExpenseDTO); //by-reference?
        return expenseRepository.save(newExpense);
    }

    @PostMapping("/earnings")
    public Earning createEarning (@RequestBody EarningDTO newEarningDTO) {
        Earning newEarning = new Earning();
        updateEarningFromDTO(newEarning, newEarningDTO); //by-reference?
        return earningRepository.save(newEarning);
    }

    @PostMapping("/financialTransactionRecords")
    public void createFinancialTransactionRecords(@RequestBody FinancialRecord financialRecord) {
        financialRecordRepository.save(financialRecord);
    }

    //Get-Services
    @GetMapping("/financial-records")
    public Iterable<FinancialRecord> getAllFinancialRecords() {
        return financialRecordRepository.findAll();
    }

    @GetMapping("/findAllOrderByDate")
    public List<FinancialRecord> getFinancialRecordsByDate() {
        return financialRecordRepository.findAllByOrderByDateDesc();
    }

    @GetMapping("/expenses")
    public Iterable<Expense> getAllExpenses() {
        return expenseRepository.findAll();
    }

    @GetMapping("/earnings")
    public Iterable<Earning> getAllEarnings() {
        return earningRepository.findAll();
    }

    @GetMapping("/financialTransactionRecords")
    public Iterable<FinancialRecord> getAll() {
        return financialRecordRepository.findAll();
    }

    @GetMapping("/financial-records/{id}")
    public FinancialRecord one(@PathVariable Long id) {
        return financialRecordRepository.findById(id).orElseThrow(() -> new FinancialRecordNotFound(id));
    }


    //Delete-Services:
    @DeleteMapping("/financial-records/{id}")
    public void delete(@PathVariable Long id) {
        financialRecordRepository.deleteById(id);
    }

}
