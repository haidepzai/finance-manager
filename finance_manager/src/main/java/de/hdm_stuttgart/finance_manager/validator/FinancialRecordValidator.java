package de.hdm_stuttgart.finance_manager.validator;

import de.hdm_stuttgart.finance_manager.model.FinancialRecord;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.math.BigDecimal;
import java.util.Date;

public class FinancialRecordValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return FinancialRecord.class.equals(aClass);
    }
    @Override
    public void validate(Object o, Errors errors) {
        if(o instanceof FinancialRecord fr) {
            if(isNull(((FinancialRecord) o).getDate())) { //Casten, weil o ein Object von FinancialRecord ist
                errors.rejectValue("date", "empty");
            }
            if(isNull(fr.getAmount())) {
                errors.rejectValue("amount", "empty");
            }
            if(isNegative(fr.getAmount())) {
                errors.rejectValue("amount", "negative");
            }
            if(isEmptyString(fr.getItem())) {
                errors.rejectValue("item", "empty");
            }
        }
    }

    boolean isEmptyString(String item) {
        return (item == null || item.equals(""));
    }

    boolean isNegative(BigDecimal amount) {
        return (amount.doubleValue() < 0.00);
    }

    boolean isNull(BigDecimal amount) {
        return (amount == null);
    }

    boolean isNull(Date date) {
        return (date == null);
    }
}
