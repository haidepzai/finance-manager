import { Earning } from './../../../interfaces/earning.interface';
import { EARNINGS_MOCK } from './../../../constants/earning.constants';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-earnings',
  templateUrl: './earnings.component.html',
  styleUrls: ['./earnings.component.scss']
})
export class EarningsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  displayedColumns: string[] = ['id', 'label', 'source', 'category', 'paymentType', 'amount', 'actions'];
  earnings = EARNINGS_MOCK;

  onAdd(): void {

  }

  onEdit(earning: Earning) {}

}
