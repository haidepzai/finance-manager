package de.hdm_stuttgart.finance_manager.repository;

import de.hdm_stuttgart.finance_manager.model.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource
public interface CategoryRepository extends CrudRepository<Category, Long> {
    List<Category> findAllByOrderByNameAsc();

    Optional<Category> findByName(String name);
}
