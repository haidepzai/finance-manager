export enum PaymentType {
    CASH,
    CREDIT_CARD,
    CARD,
    DEBIT,
    CASH_TRANSFER,
  }
  