import { EXPENSES_MOCK } from './../constants/expense.constants';
import { CreateExpenseDto, Expense, UpdateExpenseDto } from './../interfaces/expense.interface';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ExpenseService {
  private expenses: Expense[] = EXPENSES_MOCK;

  constructor() { }

  async getExpenses(): Promise<Expense[]> {
    return this.expenses;
  }

  async createExpense(expense: CreateExpenseDto): Promise<Expense> {
    const id = Math.max(...this.expenses.map((o) => o.id)) + 1;
    const item: Expense = {
      id,
      ...expense,
    };
    this.expenses.push(item);
    return item;
  }

  async updateExpense(id: number, expense: UpdateExpenseDto): Promise<Expense | undefined> {
    const item = this.expenses.find((o) => o.id === id);
    if (item) {
      Object.assign(item, expense);
      return item;
    }
    return undefined;
  }

  async deleteExpense(id: number): Promise<void> {
    this.expenses.splice(
      this.expenses.findIndex((o) => o.id === id),
      1
    );
  }
}
