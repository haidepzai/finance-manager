import { PaymentType } from '../enums/payment-type.enum';
import { Earning } from '../interfaces/earning.interface';
import { CATEGORIES_MOCK } from './category.constants';

export const EARNINGS_MOCK: Earning[] = [
  {
    id: 1,
    label: 'Gehalt',
    date: new Date('2021-06-31'),
    amount: 2500.00,
    category: CATEGORIES_MOCK[4],
    paymentType: PaymentType.CASH_TRANSFER,
    source: 'Best AG',
  },
  {
    id: 2,
    label: 'Nebenjob Lohn',
    date: new Date('2020-06-15'),
    amount: 450.0,
    category: CATEGORIES_MOCK[4],
    paymentType: PaymentType.CASH_TRANSFER,
    source: 'Freelance',
  },
  {
    id: 3,
    label: 'Gehalt',
    date: new Date('2020-05-31'),
    amount: 2500.00,
    category: CATEGORIES_MOCK[4],
    paymentType: PaymentType.CASH_TRANSFER,
    source: 'Best AG',
  },
];
