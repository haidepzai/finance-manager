package de.hdm_stuttgart.finance_manager.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(
        name="category",
        uniqueConstraints=@UniqueConstraint(columnNames={"name"})
)
@EntityListeners(DateEntityListener.class)
public class Category extends DateEntity {

    @Column(nullable = false, unique = true)
    @NotNull(message="Name der Kategorie muss angegeben werden!")
    @Size(min = 1, message="Name der Kategorie muss mindestens ein Zeichen lang sein.")
    private String name;

    public Category(){}

    public Category(String name){
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                ", name='" + name + '\'' +
                '}';
    }
}
