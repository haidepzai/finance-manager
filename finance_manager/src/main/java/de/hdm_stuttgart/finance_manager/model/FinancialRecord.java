package de.hdm_stuttgart.finance_manager.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="discriminator")
@EntityListeners(AuditingEntityListener.class)
public abstract class FinancialRecord extends DateEntity {

    @Temporal(TemporalType.DATE)
    @NotNull(message="Datum muss angegeben werden.")
    private Date date;

    @NotNull(message="Betrag muss angegeben werden.")
    @DecimalMin(value="0.01", message="Betrag muss mindestens 0.01 sein.")
    private BigDecimal amount;

    @NotNull(message="Gegenstand muss angegeben werden.")
    @Size(min=1, message="Gegenstand muss mindestens 1 Zeichen lang sein.")
    private String item;

    @ManyToOne(optional = false)
    private Category category;

    @Enumerated(EnumType.STRING)
    @NotNull(message="Zahlungsart muss angegeben werden.")
    private PaymentMethod paymentMethod;

    public FinancialRecord(
            Date date,
            BigDecimal amount,
            String item,
            Category category,
            PaymentMethod paymentMethod
    ) {
        this.date = date;
        this.amount = amount;
        this.item = item;
        this.category = category;
        this.paymentMethod = paymentMethod;
    }

    public FinancialRecord() {}

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String purpose) {
        this.item = purpose;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @Override
    public String toString() {
        return "FinancialRecord{" +
                ", date=" + date +
                ", amount=" + amount +
                ", item='" + item +
                ", category=" + category +
                ", paymentMethod=" + paymentMethod +
                '}';
    }

}
