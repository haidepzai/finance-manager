package de.hdm_stuttgart.finance_manager.repository;

import de.hdm_stuttgart.finance_manager.model.Earning;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface EarningRepository extends CrudRepository<Earning, Long> {
    List<Earning> findByItem(String item);

}
