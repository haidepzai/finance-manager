package de.hdm_stuttgart.finance_manager.dto;

import de.hdm_stuttgart.finance_manager.model.PaymentMethod;

import java.math.BigDecimal;
import java.util.Date;

public class EarningDTO extends FinancialRecordDTO {
    public EarningDTO(
            Date date,
            BigDecimal amount,
            String recipient,
            String item,
            Long categoryId,
            PaymentMethod paymentMethod
    ) {
        super(date, amount, item, categoryId, paymentMethod);
        this.source = recipient;
    }

    private String source;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
