import { Injectable } from '@angular/core';
import { StorageKey } from '../enums/storage-key.enum';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  constructor() {}

  set(key: StorageKey, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  get(key: string): any {
    const value = localStorage.getItem(key);
    if (value) {
      return JSON.parse(value);
    }
    return null;
  }

  remove(key: string): any {
    localStorage.removeItem(key);
  }
}
